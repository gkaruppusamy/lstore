<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntityMeta extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'entity_id', 'meta_key', 'meta_value'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'entity_id', 'created_at', 'updated_at'
    ];

    /**
     * Get a meta by key
     *
     */
    public function scopeGetByKey($query, $entityId, $key) {
    	return $query->whereEntityId($entityId)->whereMetaKey($key)->first();
    }
}
