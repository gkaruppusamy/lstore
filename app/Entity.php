<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Term;

class Entity extends Model
{
	/**
	 * Author taxonomy ID
	 *
	 */
	public $author_tax_id = 1;

	/**
	 * Genre taxonomy ID
	 *
	 */
	public $genre_tax_id = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'entity_type_id', 'name', 'description', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'entity_type_id', 'status', 'updated_at'
    ];

    /**
     * Entity meta
     *
     */
    public function meta() {
        return $this->hasMany('App\EntityMeta', 'entity_id', 'id');
    }

    /**
     * Getting the related terms
     *
     */
    public function terms() {
        return $this->hasMany('App\EntityTerm', 'entity_id', 'id');
    }

    /**
     * Author of an entity (Book)
     *
     */
    public function author() {
    	return Term::join('entity_terms', 'entity_terms.term_id', '=', 'terms.id')
    		->where('entity_terms.entity_id', '=', $this->id)
    		->where('terms.tax_id', '=', $this->author_tax_id)
    		->select('entity_terms.id as entity_term_id', 'terms.*')
    		->first();
    }

    /**
     * Genre of an entity (Book)
     *
     */
    public function genre() {
    	return Term::join('entity_terms', 'entity_terms.term_id', '=', 'terms.id')
    		->where('entity_terms.entity_id', '=', $this->id)
    		->where('terms.tax_id', '=', $this->genre_tax_id)
    		->select('entity_terms.id as entity_term_id', 'terms.*')
    		->first();
    }
}
