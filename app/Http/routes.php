<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/** 
 * API Routes
 *
 */
Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {
	Route::get('term/authors', 'TermController@authors');
	Route::get('term/genres', 'TermController@genres');

	Route::resource('book', 'EntityController');
	Route::resource('term', 'TermController');
});
