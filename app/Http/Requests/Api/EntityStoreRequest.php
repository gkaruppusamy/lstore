<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiRequest;

class EntityStoreRequest extends ApiRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'entity_type_id' => 'required|exists:entity_types,id',
            'name' => 'required',
            'description' => 'required',
            'terms' => 'required|array'
        ];
    }
}
