<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiRequest;

class TermStoreRequest extends ApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tax_id' => 'required|exists:taxonomies,id',
            'name' => 'required'
        ];
    }
}
