<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\ApiController;

use App\Http\Requests\Api\TermStoreRequest;

use App\Entity;
use App\Term;

use App\Transformers\TermTransformer;

class TermController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return 'calling index';
    }

    /**
     * Get the all authors
     *
     */
    public function authors(Entity $entity) {
        $authors = Term::where('tax_id', $entity->author_tax_id)->get();

        return $this->responseCollection($authors, new TermTransformer);
    }

    /**
     * Get the all genres
     *
     */
    public function genres(Entity $entity) {
        $genres = Term::where('tax_id', $entity->genre_tax_id)->get();

        return $this->responseCollection($genres, new TermTransformer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TermStoreRequest $request)
    {
        $input = $request->all();

        $term = Term::create($input);

        return $this->responseItem($term, new TermTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TermStoreRequest $request, $id)
    {
        $input = $request->all();

        $term = Term::find($id);

        if (!isset($term->id)) {
            $errors = ['Invalid term'];
            return $this->responseErrorArray($errors);
        }

        $term->update($input);

        return $this->responseItem($term, new TermTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
