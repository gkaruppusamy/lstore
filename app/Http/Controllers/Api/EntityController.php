<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\ApiController;

use App\Http\Requests\Api\EntityStoreRequest;

use App\Entity;
use App\EntityTerm;
use App\EntityMeta;

use App\Transformers\EntityTransformer;

class EntityController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection = Entity::orderBy('created_at', 'desc')->get();

        return $this->responseCollection($collection, new EntityTransformer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EntityStoreRequest $request)
    {
        $input = $request->except(['terms', 'meta']);

        $entity = Entity::create($input);

        // Save the entity terms relations
        if (is_array($request->input('terms')))
            $termArr = $request->input('terms');
        else
            $termArr = json_decode($request->input('terms'));

        foreach ($termArr as $t_id) {
            $entityTerm = EntityTerm::create([
                'term_id' => $t_id,
                'entity_id' => $entity->id
            ]);
        }

        // Meta data
        foreach ($request->input('meta') as $key => $value) {
            EntityMeta::create([
                'entity_id' => $entity->id,
                'meta_key' => $key,
                'meta_value' => $value
            ]);
        }

        $meta = [
            'messages' => ['Book has been added']
        ];

        return $this->responseItem($entity, new EntityTransformer, $meta);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entity = Entity::find($id);

        if (!isset($entity->id)) {
            $errors = ['Invalid entity'];
            return $this->responseErrorArray($errors);
        }

        return $this->responseItem($entity, new EntityTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EntityStoreRequest $request, $id)
    {

        $input = $request->except(['terms', 'meta']);

        $entity = Entity::find($id);

        if (!isset($entity->id)) {
            $errors = ['Invalid entity'];
            return $this->responseErrorArray($errors);
        }

        $entity->update($input);

        // Update the entity terms relations
        if (is_array($request->input('terms')))
            $termArr = $request->input('terms');
        else
            $termArr = json_decode($request->input('terms'));

        foreach ($termArr as $p_id => $t_id) {
            if (is_array($t_id)) // unchanged
                continue;

            if ($p_id == 'author')
                $term = $entity->author();

            if ($p_id == 'genre')
                $term = $entity->genre();

            if (isset($term->id)) {
                $entityTerm = EntityTerm::find($term->entity_term_id);

                $entityTerm->update([
                    'term_id' => $t_id
                ]);
            } else {
                EntityTerm::create([
                    'entity_id' => $entity->id,
                    'term_id' => $t_id
                ]);
            }
        }

        // Meta update
        foreach ($request->input('meta') as $key => $value) {
            $entityMeta = EntityMeta::getByKey($entity->id, $key);

            if (isset($entityMeta->id)) {
                if ($entityMeta->meta_value != $value) {
                    $entityMeta->meta_value = $value;
                    $entityMeta->save();
                }
            } else {
                $entityMeta = EntityMeta::create([
                    'entity_id' => $entity->id,
                    'meta_key' => $key,
                    'meta_value' => $value
                ]);
            }
        }

        $meta = [
            'messages' => ['Book has been updated']
        ];

        return $this->responseItem($entity, new EntityTransformer, $meta);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
