<?php

namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class ApiController extends ApiGuardController {

    use ValidatesRequests;


     /**
     * Default success data
     *
     * @var array
     */
    protected $successData = [
        'success' => true,
        'messages' => []
    ];

    /**
     * API response for array
     *
     */
    public function responseArray($arr = array(), $meta = array()) {
        $meta = array_merge($this->successData, $meta);

        $arr = [
            'data' => $arr,
            'meta' => $meta
        ];

        return $this->response->withArray($arr);
    }

    /**
     * API response for errors
     *
     */
    public function responseErrorArray($errors = array(), $isStatus200 = false) {
        $arr['meta'] = [
            'success' => false,
            'messages' => $errors
        ];
		
		$statusCode = ($isStatus200) ? 200 : config('constants.api_error_code');
		
        return $this->response->setStatusCode($statusCode)->withArray($arr);
    }

    /**
     * API response for model item
     *
     */
    public function responseItem($item, $transformer, $meta = array()) {
        $meta = array_merge($this->successData, $meta);

        return $this->response->withItem($item, $transformer, null, $meta);
    }

    /**
     * API response for model collection
     *
     */
    public function responseCollection($collection, $transformer, $meta = array()) {
        $meta = array_merge($this->successData, $meta);

        return $this->response->withCollection($collection, $transformer, null, null, $meta);
    }

    /**
     * API response for model collection with pagination
     *
     */
    public function responsePaginator($collection, $transformer, $meta = array()) {
        $meta = array_merge($this->successData, $meta);

        return $this->response->withPaginator($collection, $transformer, null, $meta);
    }

}