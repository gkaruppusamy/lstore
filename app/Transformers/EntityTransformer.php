<?php
namespace App\Transformers;

use League\Fractal;

use App\Entity;
use App\EntityMeta;

class EntityTransformer extends Fractal\TransformerAbstract
{
	public function transform(Entity $entity)
	{
	    $return = $entity->toArray();

	    $author = $entity->author();
	    if (isset($author->id))
	    	$return['terms']['author'] = $author;

	    $genre = $entity->genre();
	    if (isset($genre->id))
	    	$return['terms']['genre'] = $genre;

	    $priceMeta = EntityMeta::getByKey($entity->id, 'price');
	    if (isset($priceMeta->id))
	    	$return['meta']['price'] = (int)$priceMeta->meta_value;

	    $return['created_at'] = date('d M, Y', strtotime($return['created_at']));
		
		return $return;
	}
}