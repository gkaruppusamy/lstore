<?php

return [
	
	/**
	 * Http code on API error
	 */
	'api_error_code' => 422,
	
];