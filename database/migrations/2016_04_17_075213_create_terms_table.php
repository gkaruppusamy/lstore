<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tax_id')->nullable()->unsigned();
            $table->string('name');
            $table->text('description');
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('tax_id')
              ->references('id')->on('taxonomies')
              ->onDelete('cascade')
              ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('terms');
    }
}
