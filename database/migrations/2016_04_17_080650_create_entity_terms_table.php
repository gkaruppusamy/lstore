<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_terms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('term_id')->nullable()->unsigned();
            $table->integer('entity_id')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('term_id')
              ->references('id')->on('terms')
              ->onDelete('cascade')
              ->onUpdate('no action');

            $table->foreign('entity_id')
              ->references('id')->on('entities')
              ->onDelete('cascade')
              ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('entity_terms');
    }
}
