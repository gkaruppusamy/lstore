<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEntityTypeToEntities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('entities', function (Blueprint $table) {

            $table->integer('entity_type_id')->after('id')->nullable()->unsigned();

            $table->foreign('entity_type_id')
              ->references('id')->on('entity_types')
              ->onDelete('cascade')
              ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entities', function (Blueprint $table) {
            $table->dropForeign(['entity_type_id']);
            
            $table->dropColumn('entity_type_id');
        });
    }
}
