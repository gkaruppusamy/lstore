<?php

use Illuminate\Database\Seeder;

use App\Term;

class TermsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $terms = [[
        	'author' => 'A. A. Milne',
        	'genre' => 'Children Literature'
        ], [
        	'author' => 'Agatha Christie',
        	'genre' => 'Crime Novels'
        ], [
        	'author' => 'Alan Moore',
        	'genre' => 'Science fiction, Non-Fiction, Superhero, Horror'
        ], [
        	'author' => 'Albert Camus',
        	'genre' => 'Absurdist Fiction, Existentialism'
        ], [
        	'author' => 'Barbara Cartland',
        	'genre' => 'Romance'
        ], [
        	'author' => 'C. S. Lewis',
        	'genre' => 'Fantasy, Popular Theology'
        ], [
        	'author' => 'Carol Ann Duffy',
        	'genre' => 'Plays, Poetry'
        ]];

        foreach ($terms as $arr) {
        	foreach ($arr as $key => $value) {
        		$t_id = ($key == 'author') ? 1 : 2;
        		$desc = ($key == 'author') ? 'Author' : 'Genre';

        		Term::create([
	        		'tax_id' => $t_id,
	        		'name' => $value,
	        		'description' => $desc
	        	]);
        	}
        }
    }
}
