<?php

use Illuminate\Database\Seeder;

class TaxonomySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $taxonomies = [[
        	'name' => 'Author',
        	'description' => 'Author of the book'
        ], [
        	'name' => 'Genre',
        	'description' => 'Genre of the book'
        ]];

        foreach ($taxonomies as $arr) {
        	DB::table('taxonomies')->insert($arr);
        }
    }
}
