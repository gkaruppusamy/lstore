<?php

use Illuminate\Database\Seeder;

class EntityTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('entity_types')->insert([
        	'name' => 'book',
        	'description' => 'Book entity'
        ]);
    }
}
